
;;
; @defgroup MemSet Memory fill functions (ROM0)
; Fills areas of memory with a constant value.
; @{
;
; @param hl Start address
;;

;;
; Fill memory with A continuously starting from HL
; for BC bytes.
;
; @param a Value to fill memory with
; @param bc How many bytes to fill
; @warning `bc` must not be $0000!
;;
	XDEF MemSet16
;;
; Fill memory with A continuously starting from HL
; for C bytes.
;
; @param a Value to fill memory with
; @param c How many bytes to fill
; @warning `c` must not be $00!
;;
	XDEF MemSet8
;;
; Fills a portion of memory with D starting at
; HL for BC bytes with waiting for VRAM
;
; @param d Value to fill memory with
; @param bc How many bytes
; @clobber a, d
;;
	XDEF VideoMemSet
;;
; @}
;;

;;
; @defgroup MemCpy Memory copying functions (ROM0)
; Copies portions of memory from one place to another.
; @{
;
; @param hl Destination start address
; @param de Source start address
;;

;;
; Copies a portion of memory from DE to HL for
; BC bytes.
;
; @param bc How many bytes
; @clobber a
;;
	XDEF MemCpy16
;;
; Copies a portion of memory from DE to HL for
; C bytes.
;
; @param c How many bytes
; @clobber a
;;
	XDEF MemCpy8
;;
; Copies a portion of memory with waiting
; for VRAM to be accessible.
;
; @param bc How many bytes
; @clobber a
;;
	XDEF VideoMemCpy
;;
; @}
;;

;;
; @defgroup LcdFuncs LCD manipulation (ROM0)
; @{
; Manipulating the Game Boy's display.
;;

;;
; Turns off the screen, employing a manual VBlank wait.
;;
	XDEF DisableLCD
;;
; Turns on the screen using the value in @ref hLCDSettings.
;
; @param hLCDSettings value for rLCDC
; @clobber a
;
; @warning The screen must be turned off when calling this!
;;
	XDEF EnableLCD
;;
; @}
;;

;;
; @defgroup Timing Timing functions (ROM0)
; @{
; Waiting for stuff to happen.
;;

;;
; Waits for VBlank to hit by halting.
;
; @clobber a
;;
	XDEF DelayFrame
;;
; Delay a certain amount of frames.
;
; @param c amount of frames to wait
;;
	XDEF DelayFrames
;;
; @}
;;

;;
; Go to a jumptable entry.
; 
; @param hl Jumptable address containing pointers
; @param a Entry number
;
; @clobber de
;;
	XDEF GotoJumptableEntry
;;
; Poll joypad input
;
; @return hInput Updated joypad state
;;
	XDEF GetJoypad
;;
; @param hl Destination address
; @param b  Height
; @param c  Width
; @param d  Starting tile ID
;
; @clobber a
;;
	XDEF DrawPicTilemap
;;
; Copies a string to VRAM.
;
; @param hl Where to print the string
; @param de Pointer to string
;
; @clobber b, c
;;
	XDEF PrintString
;;
; @defgroup PalFade Palette fading animations (ROM0)
; @{
;;

;;
; Smoothly fade out to white
;
; @clobber a, c
;;
	XDEF WhiteOut
;;
; Smoothly fade in from white
;
; @clobber a, c
;;
	XDEF WhiteIn
;;
; Smoothly fade out to black
;
; @clobber a, c
;;
	XDEF BlackOut
;;
; Smoothly fade in from black
;
; @clobber a, c
;;
	XDEF BlackIn
;;
; @}
;;

;;
; Fills the BG map with A.
;
; @param a Tile ID to fill the screen with
;;
	XDEF ClearScreen