	section "hram", HRAM

;;
; Which Game Boy model this game is being run on.
;;
hGBType:: db

;;
; OAM DMA routine.
;;
hDMACode:: ds 10

;;
; Current screen mode
;;
hLCDSettings:: db

;;
; VBlank flag. Will be 0 when VBlank is hit.
;;
hAskVBlank:: db

;;
; Current joypad state.
;;
hInput:: db