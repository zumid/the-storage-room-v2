	include "hardware.inc"
	include "macros.inc"
	include "constants.inc"

	section "Title", CODE, BANK[1]

GM_Title_init::
	XREF LoadAlphabet
	call LoadAlphabet

	XREF VideoMemSet, VideoMemCpy
	XREF ClearScreen

	ld a, $ff
	call ClearScreen

; black out
	ld a, %11111111
	ld [rBGP], a

	XREF GFX_TS_buster, GFX_TS_buster_END
	XREF GFX_TS_buddy, GFX_TS_buddy_END
	XREF DrawPicTilemap

; load title GFX
	ld hl, vChars0
	ld bc, GFX_TS_buddy_END - GFX_TS_buddy
	ld de, GFX_TS_buddy
	call VideoMemCpy

; Place title gfx
	VRAM_COORD hl, vBGMap0, 0, 4
	ld bc, $0C09
	ld d,  0
	call DrawPicTilemap

	XREF title0, title1, title2, title3
	XREF PrintString

; THE STORAGE ROOM
	VRAM_COORD hl, vBGMap0, 0, 0
	ld de, title0
	call PrintString

; VERSION xx
	VRAM_COORD hl, vBGMap0, 0, 1
	ld de, title1
	call PrintString

; PRESS START
	VRAM_COORD hl, vBGMap0, 9, 10
	ld de, title2
	call PrintString

; (c)
	VRAM_COORD hl, vBGMap0, 3, 17
	ld de, title3
	call PrintString

	XREF wScreenDistortionEffect
	
; enable screen dist FX
	ld a, 1
	ld [wScreenDistortionEffect], a

	XREF BlackIn
	call BlackIn

	ret

GM_Title_loop::
	XREF hInput
	
	ldh a, [hInput]
	bit BUTTONF_START, a
	ret z

	XREF wCurrentGameMode

	ld a, GM_MENU
	ld [wCurrentGameMode], a
	ret

