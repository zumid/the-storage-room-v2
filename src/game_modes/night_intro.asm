	include "hardware.inc"
	include "macros.inc"
	include "constants.inc"

	section "Night intro", CODE, BANK[1]

GM_NightIntro_init::
	xor a
	ld [rBGP], a

	XREF wScreenDistortionEffect
	ld a, 1
	ld [wScreenDistortionEffect], a

	XREF ClearScreen
	ld a, $ff
	call ClearScreen

	XREF nightstr, PrintString

	VRAM_COORD hl, vBGMap0, 6, 8
	ld de, nightstr
	call PrintString

	XREF wNight
	ld a, [wNight]
	inc a
	add $EA ; "0"
	ld c, a

	VRAM_COORD hl, vBGMap0, 13, 8
	WAIT_VRAM
	ld [hl], c

	XREF WhiteIn
	call WhiteIn

	XREF DelayFrames
	ld c, 5
	call DelayFrames

	xor a
	ld [wScreenDistortionEffect], a

	ld c, 75
	call DelayFrames

	XREF BlackOut
	call BlackOut
	ret

GM_NightIntro_loop::
	XREF wCurrentGameMode
	ld a, GM_OFFICE
	ld [wCurrentGameMode], a
	ret