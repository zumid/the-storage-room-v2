	XREF GM_Title_init, GM_Title_loop
	XREF GM_Menu_init, GM_Menu_loop
	XREF GM_NightIntro_init, GM_NightIntro_loop
	XREF GM_Office_init, GM_Office_loop
	
Modes_Init_Jumptable::
	dw GM_Title_init
	dw GM_Menu_init
	dw GM_NightIntro_init
	dw GM_Office_init

Modes_Loop_Jumptable::
	dw GM_Title_loop
	dw GM_Menu_loop
	dw GM_NightIntro_loop
	dw GM_Office_loop