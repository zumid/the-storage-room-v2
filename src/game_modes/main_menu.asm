	include "hardware.inc"
	include "macros.inc"

	section "Menu mode", CODE, BANK[1]

GM_Menu_init::
	XREF hInput
	xor a
	ldh [hInput], a

	XREF BlackOut, BlackIn
	call BlackOut

	XREF wScreenDistortionEffect
	xor a
	ld [wScreenDistortionEffect], a

	XREF ClearScreen
	ld a, $ff
	call ClearScreen

	XREF menu0, PrintString

	VRAM_COORD hl, vBGMap0, 5, 5
	ld de, menu0
	call PrintString

	call BlackIn
	ret

GM_Menu_loop::
	ldh a, [hInput]
	bit BUTTONF_START, a
	ret z

	XREF wCurrentGameMode

	ld a, GM_NIGHTINTRO
	ld [wCurrentGameMode], a
	ret