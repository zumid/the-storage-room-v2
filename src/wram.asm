	include "constants.inc"

	section "wram", BSS

;;
; Allocated stack.
;;
wStack:: ds $100
wStackTop::

;;
; Sprite buffer, will be copied to the OAM at VBlank
;;
wVirtualOAM:: ds MAX_SPRITES * 4

;;
; Which game mode is currently running
;;
wCurrentGameMode:: db

;;
; Which game mode are we transitioning from
;;
wPreviousGameMode:: db

;;
; Whether or not the screen distortion effect is executed
;
; @retval 0 Disable effect
; @retval 1 Enable effect 
;;
wScreenDistortionEffect:: db

;;
; Current night number. Determines animatronic aggressiveness.
;;
wNight:: db