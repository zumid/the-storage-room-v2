	include "hardware.inc"
	include "constants.inc"

	XREF VideoMemCpy, VideoMemSet

	section "load_gfx", HOME
;;
; Loads the tiles for the alphabet and a
; blank tile at $FF in vChars0
;
; @clobber a, hl, bc, de
;;
LoadAlphabet::
	XREF GFX_Alphabet, GFX_Alphabet_END
	ld de, GFX_Alphabet
	ld bc, GFX_Alphabet_END - GFX_Alphabet
	ld hl, vChars0 + $d0 tiles
	jp VideoMemCpy