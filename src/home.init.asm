	include "hardware.inc"
	include "constants.inc"
	
	section "entry_point", HOME[$100]
	nop
	jp Init
	
	section "header", HOME[$134]
	db "STORAGE ROOM   "
	db $00 ; cgb enabled
	db "ZD" ; new licensee code
	db $00 ; sgb enable
	db $12 ; cart type
	db $00 ; rom size
	db $00 ; ram size
	db $01 ; international
	db $33 ; use new code
	db $00 ; rom version
	db 0   ; header chksum, handled by linker
	dw 0   ; global chksum, handled by linker

	section "init", HOME[$150]

;;
; Entry point for the entire game.
;;
Init::
	XREF hGBType, wStackTop, wVirtualOAM, hDMACode
	XREF DisableLCD, EnableLCD, hLCDSettings
	XREF MemSet16

	di
	ld b, a

; at this point we don't use the utility functions
; because we haven't set sp yet

; clear HRAM
	ld d, $ffff-$ff80
	xor a
	ld c, $80
.clrhram
	ld [c], a
	inc c
	dec d
	jr nz, .clrhram

; store GB version
	ld a, b
	ldh [hGBType], a
	
	call DisableLCD

; clear WRAM
	ld hl, $c000
	ld bc, $e000-$c000
.clrwram
	xor a
	ld [hl+], a
	dec bc
	ld a, b
	or c
	jr nz, .clrwram

	ld sp, wStackTop

; clear VRAM
	ld hl, $8000
	ld bc, $a000-$8000
	call MemSet16
	
; copy DMA routine
	ld hl, DMA_Routine
	ld b, DMA_Routine_end - DMA_Routine
	ld c, hDMACode ~/ $ff ; LOW(hDMACode)
.copy_code
	ld a, [hl+]
	ld [c], a
	inc c
	dec b
	jr nz, .copy_code

; force initialization
	XREF wPreviousGameMode, wCurrentGameMode
	ld a, -1
	ld [wPreviousGameMode], a
	xor a ; ld a, GM_TITLE
	ld [wCurrentGameMode], a
	
; set LCD setting
	ld a, LCD_ENABLE | LCD_SET_8000 | LCD_MAP_9800 | LCD_OBJ_NORM | LCD_OBJ_ENABLE | LCD_BG_ENABLE
	ldh [hLCDSettings], a
	call EnableLCD

; reset palettes
	ld a, %11100100
	ldh [rBGP], a
	ldh [rOBP0], a
	ldh [rOBP1], a
	
; enable vblank
	ld a, (1 << VBLANK) | (1 << LCD_STAT)
	ldh [rIE], a

; specify hblank interrupt
	ld a, (1 << 3)
	ldh [rSTAT], a
	ei

MainGameLoop::
	XREF GotoJumptableEntry
	XREF DelayFrame, GetJoypad

.loop
	ld hl, wPreviousGameMode
	ld a, [wCurrentGameMode]
	cp [hl]
	jr z, .skip_init

	ld hl, Modes_Init_Jumptable
	call GotoJumptableEntry
	
	ld a, [wCurrentGameMode]  ; reload game mode
	ld [wPreviousGameMode], a  ; replace old game mode

.skip_init
	ld hl, Modes_Loop_Jumptable
	call GotoJumptableEntry
	
	call DelayFrame
	call GetJoypad
	jp .loop

;;
; Placed in hDMACode. To be run every VBlank, copies
; wVirtualOAM to GB OAM.
;;
DMA_Routine::
	ld a, wVirtualOAM // $1000000 ; HIGH(wVirtualOAM)
	ldh [rDMA], a
	ld a, MAX_SPRITES
.loop
	dec a
	jr nz, .loop
	ret
DMA_Routine_end:

	INCLUDE "game_modes/game_modes.inc"