	include "hardware.inc"

; interrupt vectors
	section "rst00", HOME[0]
	ret
	
	section "rst08", HOME[8]
	ret
	
	section "rst10", HOME[$10]
	ret
	
	section "rst18", HOME[$18]
	ret
	
	section "rst20", HOME[$20]
	ret
	
	section "rst28", HOME[$28]
	ret
	
	section "rst30", HOME[$30]
	ret
	
	section "rst38", HOME[$38]
	ret
	
	section "int_vblank", HOME[$40]
	jp iVBlank
	
	section "int_hblank", HOME[$48]
	jp iLCDStat
	
	section "int_timer", HOME[$50]
	reti
	
	section "int_serial", HOME[$58]
	reti
	
	section "int_joypad", HOME[$60]
	reti

; interrupt contents
	section "ints", HOME
iVBlank::
	XREF hAskVBlank
	XREF hDMACode
	push af
		xor a
		ldh [hAskVBlank], a

		call hDMACode ; update sprites
	pop af
	reti

iLCDStat::
	XREF wScreenDistortionEffect
	push af
	ld a, [wScreenDistortionEffect]
	and a
	jr z, .zero
	ld a, [rDIV]
	cp $80
	jr c, .zero
		ldh a, [rDIV]
		and %00000011
		cp  %00000001
		jr nc, .skip
.zero
		xor a
.skip
		ldh [rSCX], a
	pop af
	reti
