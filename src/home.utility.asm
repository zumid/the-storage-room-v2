	include "hardware.inc"
	include "macros.inc"
	include "constants.inc"

	include "home.utility.inc"
	section "utility", HOME

MemSet16:
	dec bc
	inc b
	inc c
.loop
	ld [hl+], a
	dec c
	jr nz, .loop
	dec b
	jr nz, .loop
	ret z
	jr .loop

MemSet8:
.loop
	ld [hl+], a
	dec c
	jr nz, .loop
	ret

VideoMemSet:
	dec bc
	inc b
	inc c
.loop
	WAIT_VRAM
	ld [hl], d
	inc hl
	dec c
	jr nz, .loop
	dec b
	ret z
	jr .loop

MemCpy16:
	dec bc
	inc b
	inc c
.loop
	ld a, [de]
	inc de
	ld [hl+], a
	dec c
	jr nz, .loop
	dec b
	ret z
	jr .loop

MemCpy8:
.loop
	ld a, [de]
	inc de
	ld [hl+], a
	dec c
	jr nz, .loop
	ret

VideoMemCpy:
	dec bc
	inc b
	inc c
.loop
	WAIT_VRAM
	ld a, [de]
	inc de
	ld [hl+], a
	dec c
	jr nz, .loop
	dec b
	ret z
	jr .loop

DisableLCD:
.loop
	ld a, [rLY]
	cp LY_VBLANK
	jr nc, .disable ; >= LY_VBLANK
	jr .loop
.disable
	ld a, [rLCDC]
	res LCD_ENABLE_BIT, a ; disable LCD
	ld [rLCDC], a
	ret

EnableLCD:
	XREF hLCDSettings
	ldh a, [hLCDSettings]
	ld [rLCDC], a
	ret

DelayFrame:
	XREF hAskVBlank
	ld a, 1
	ldh [hAskVBlank], a
.loop
	halt
	nop
	ldh a, [hAskVBlank]
	and a
	ret z  ; exit if vblank is acknowledged
	jr .loop

DelayFrames:
.loop
	call DelayFrame
	dec c
	jr nz, .loop
	ret

GotoJumptableEntry:
	ld e, a
	ld d, 0
	add hl, de
	add hl, de
	ld a, [hl+]
	ld h, [hl]
	ld l, a
	jp [hl]

GetJoypad:
	XREF hInput
	ld a, 1 << JOYP_SELECT_NOT_BUTTONS
	ldh [rJOYP], a
	rept 4
	ldh a, [rJOYP]
	endr
	cpl       ; flip all the bits
	and %1111 ; get only lower half
	swap a    ; make it the upper half
	ld b, a   ; store to b
	ld a, 1 << JOYP_SELECT_NOT_DPAD
	ldh [rJOYP], a
	REPT 4
	ldh a, [rJOYP]
	ENDR
	cpl              ; flip all the bits
	and %1111        ; get only lower half
	or b             ; merge with the d-pad input earlier
	ldh [hInput], a  ; save
	ld a, 1 << JOYP_SELECT_NOT_BUTTONS | (1 << JOYP_SELECT_NOT_DPAD)
	ldh [rJOYP], a
	ret

DrawPicTilemap:
	push bc
.transfer_x
	WAIT_VRAM
	ld [hl], d
	inc hl
	inc d
	dec c
	jr nz, .transfer_x
	pop bc
	push bc
.go_back
	dec hl
	dec c
	jr nz, .go_back
	ld bc, BGMAP_WIDTH
	add hl, bc
	pop bc
	dec b
	push bc
	jr nz, .transfer_x
	pop bc
	ret

PrintString:
	ld c, 0 ; keep track of how many characters printed so far
.loop
	ld a, [de]
	and a
	ret z ; stop when a == 0

	inc c

	cp 1
	jr z, .new_line

	ld b, a
	WAIT_VRAM
	ld a, b
	ld [hl+], a
	inc de
	jr .loop

.new_line
; bc = -c
	ld a, c
	cpl
	inc a ; two's compl.
	inc a ; two's compl.
	ld c, a
	ld b, -1
	jr nz, .c_not_zero
	ld b, 0
.c_not_zero
	add hl, bc
	ld bc, BGMAP_WIDTH
	add hl, bc
	inc de
	ld c, 0 ; reorient text
	jr .loop

WhiteOut:
	ld a, %11100100
	call FadePaletteCommon
	ld a, %10010000
	call FadePaletteCommon
	ld a, %01000000
	call FadePaletteCommon
	xor a  ; %00000000
	jp FadePaletteCommon

WhiteIn:
	xor a  ; %00000000
	call FadePaletteCommon
	ld a, %01000000
	call FadePaletteCommon
	ld a, %10010000
	call FadePaletteCommon
	ld a, %11100100
	jp FadePaletteCommon

BlackOut:
	ld a, %11100100
	call FadePaletteCommon
	ld a, %11111001
	call FadePaletteCommon
	ld a, %11111110
	call FadePaletteCommon
	ld a, %11111111
	jp FadePaletteCommon

BlackIn:
	ld a, %11111111
	call FadePaletteCommon
	ld a, %11111110
	call FadePaletteCommon
	ld a, %11111001
	call FadePaletteCommon
	ld a, %11100100
	;jp FadePaletteCommon

FadePaletteCommon:
	ldh [rBGP], a
	ldh [rOBP0], a
	ldh [rOBP1], a
	ld c, 3
	jp DelayFrames

ClearScreen:
	push de
	VRAM_COORD hl, vBGMap0, 0, 0
	ld d, a
	ld bc, BGMAP_WIDTH*BGMAP_HEIGHT
	call VideoMemSet
	pop de
	ret
