#!/usr/bin/env python3

"""
below is the kind of docstrings it'll work with
it's admittedly very limited

;;
; Here's a Doxygen-format docstring.
;
; @param hl Target
; @param de Source
; @param bc Amount
;
; @return Clobbers:
;		- hl
;		- de
;		- bc
;;
MyLabel:
	...

;;
; Here's another Doxygen-format docstring.
;;
wMyVariable:: db

;;
; Here's another Doxygen-format docstring.
;;
SOMETHING equ 10

Use with:
FILTER_PATTERNS        = "*.asm=python3 <script dir here>/make_gbasm_docs.py" \
                         "*.inc=python3 <script dir here>/make_gbasm_docs.py"

EXTENSION_MAPPING      = asm=C \
                         inc=C

FILE_PATTERNS          = *.asm \
                         *.inc \
						...

Zumi 2023-07-03
"""
from pathlib import Path
import re
import sys

SRC_FILE = Path(sys.argv[1])

RE_IS_FUNCTION = re.compile(r"^([\w_0-9]+)::?$|^\s+(?:XDEF|EXPORT)\s+([\w_0-9]+)$")
RE_IS_DEFINE = re.compile(r"^([\w_0-9]+)\s+[Ee][Qq][Uu]\s+(.+)")
RE_IS_VARIABLE = re.compile(r"^([\w_0-9]+)::?\s+(db|ds|dw|DB|DS|DW)$|([\w_0-9]+)::?\s+(db|ds|dw|DB|DS|DW)\s+(.+)")

if __name__ == "__main__":
	got_file = SRC_FILE

	with open(got_file, "r") as file_obj:
		line = file_obj.readline()

		in_doc = False
		next_line_is_variable = False
		is_first_line = True

		while line:
			if is_first_line:
				is_first_line = False
				line = file_obj.readline()
				print("/*! @file */")
				continue

			l = line.rstrip()
			
			# parse variable name
			if next_line_is_variable:
				next_line_is_variable = False
				fvar_name = ""
				# resolve types
				if RE_IS_VARIABLE.match(l):
					_ = RE_IS_VARIABLE.match(l).groups()
					var_name = _[0] or _[2]
					dtype = _[1] or _[3]
					dsize = _[4]
					fsize = None
					ftype = "byte"
					if dtype:
						if dtype.lower() == "ds":
							fsize = dsize.strip()
						elif dtype.lower() == "dw":
							ftype = "word"
					fvar_name = "%s %s" % (ftype, var_name)
					if fsize:
						fvar_name += "[%s]" % fsize
					fvar_name += ";"
				elif RE_IS_DEFINE.match(l):
					def_name, def_value = RE_IS_DEFINE.match(l).groups()
					fvar_name = "#define %s %s" % (def_name, def_value)
				elif RE_IS_FUNCTION.match(l):
					_ = RE_IS_FUNCTION.match(l).groups()
					fun_name = _[0] or _[1]
					fvar_name = "%s();" % fun_name
				
				print(fvar_name)

				line = file_obj.readline()
				continue

			# parse documentation
			if l == ";;":
				in_doc = not in_doc
				if not in_doc:
					next_line_is_variable = True
			if in_doc or next_line_is_variable:
				if in_doc and l == ";;":
					print("/*! @brief")
				elif next_line_is_variable and l == ";;":
					print("*/")
				else:
					print(re.sub(r"^;\s*", "", l))
			else:
				print()
			line = file_obj.readline()