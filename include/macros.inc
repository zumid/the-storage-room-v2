	IFND __MACROS__
__MACROS__ equ 1

	include "hardware.inc"
	include "constants.inc"

; clobbers A
WAIT_VRAM: MACRO
.wait\@
	ldh a, [rSTAT]
	bit 1, a ; STATF_BUSY
	jr nz, .wait\@
	ENDM

; \1 register
; \2 base
; \3 x
; \4 y
VRAM_COORD: MACRO
	ld \1, \2 + \3 + (\4*BGMAP_HEIGHT)
	ENDM

	ENDC