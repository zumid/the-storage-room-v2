	IFND __CONSTANTS__
__CONSTANTS__ equ 1
;;
; Maximum number sprites the game should display.
; Set to the Game Boy's maximum
;;
MAX_SPRITES equ 40
tiles equs "* $10"
;;
; Width of the background map (in tiles)
;;
BGMAP_WIDTH equ $20
;;
; Height of the background map (in tiles)
;;
BGMAP_HEIGHT equ $20
;;
; Width of the GB screen (in tiles)
;;
SCREEN_WIDTH equ 20
;;
; Height of the GB screen (in tiles)
;;
SCREEN_HEIGHT equ 18
;;
; @defgroup hInputConst hInput constants
; @{
;;

;;
;;
BUTTONF_A      equ 0
;;
;;
BUTTONF_B      equ 1
;;
;;
BUTTONF_SELECT equ 2
;;
;;
BUTTONF_START  equ 3
;;
;;
BUTTONF_RIGHT  equ 4
;;
;;
BUTTONF_LEFT   equ 5
;;
;;
BUTTONF_UP     equ 6
;;
;;
BUTTONF_DOWN   equ 7
;;
;;
BUTTON_A      equ 1<<BUTTONF_A
;;
;;
BUTTON_B      equ 1<<BUTTONF_B
;;
;;
BUTTON_SELECT equ 1<<BUTTONF_SELECT
;;
;;
BUTTON_START  equ 1<<BUTTONF_START
;;
;;
BUTTON_RIGHT  equ 1<<BUTTONF_RIGHT
;;
;;
BUTTON_LEFT   equ 1<<BUTTONF_LEFT
;;
;;
BUTTON_UP     equ 1<<BUTTONF_UP
;;
;;
BUTTON_DOWN   equ 1<<BUTTONF_DOWN
;;
; @}
;;

;;
; @defgroup GmConst Game mode constants
; @{
;;

;;
; Title screen
;;
GM_TITLE equ 0
;;
; Main menu
;;
GM_MENU equ 1
;;
; "Night X" intro before the main game
;;
GM_NIGHTINTRO equ 2
;;
; Main game mode
;;
GM_OFFICE equ 3
;;
; @}
;;

	ENDC